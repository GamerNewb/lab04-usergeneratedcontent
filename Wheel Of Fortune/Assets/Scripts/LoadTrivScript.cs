﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;

public class LoadTrivScript : MonoBehaviour
{

    TextAsset textFile;
    TextReader reader;

    public List<string> question = new List<string>();
    public List<string> answers = new List<string>();
    public List<string> correct = new List<string>();

    private int count;
    public Text Qnum;
 

    // Use this for initialization
    void Start()
    {
        QuestionNumber();

        textFile = (TextAsset)Resources.Load("embeddedtrivia", typeof(TextAsset));
        reader = new StringReader(textFile.text);

        string lineOfText;
        int lineNumber = 0;
        count = 0;



        // the question will be every third and the questions everything in between
        while ((lineOfText = reader.ReadLine()) != null)
        {
            if (lineNumber % 4 == 0)
            {
                question.Add(lineOfText);
            }
            else if (lineNumber % 3 == 0)
            {
                correct.Add(lineOfText);
            }
            else
            {
                answers.Add(lineOfText);
            }
            lineNumber++;
        }

        SendMessage("Gather");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void QuestionNumber()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // This is annoying, i have no idea what I'm doing even though I should. I hate UI with a burning passion.
            Qnum.text = "Question # " + count.ToString() += 1;
        }
    }
}
